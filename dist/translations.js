'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var translations = {
    prefix: 'allbin_bar_',
    "sv-SE": {
        a_tool_by_allbin: 'ett verktyg av All Binary',
        account_information: 'kontoinformation',
        change_password: 'byt lösenord',
        changed: 'ändrat',
        changelog: 'versionshistorik',
        company_info_1: 'All Binary verkar inom tv\xE5 huvudsakliga aff\xE4rsomr\xE5den med den gemensamma n\xE4mnaren \n            att vi underl\xE4ttar hantering, visualisering och anv\xE4ndning av data.',
        company_info_2: 'Aff\xE4rsomr\xE5det Transport tar fasta p\xE5 att det aldrig varit viktigare \xE4n nu att resa mer kollektivt.\n            Vi underl\xE4ttar analys och planering av kollektivtrafiken genom att visualisera data fr\xE5n realtidssystem och\n            biljettsystem samt skapar verktyg f\xF6r inventering av h\xE5llplatser och \xE4rendehantering.',
        company_info_3: 'Aff\xE4rsomr\xE5det IOE med produkten SenseView har som m\xE5l att underl\xE4tta utbyggnaden av den smarta\n            staden. Vi tror p\xE5 kraften i att experimentera med m\xE5nga olika sensorer och pilotprojekt f\xF6r att skapa\n            underlag f\xF6r st\xF6rre investeringar och upphandlingar n\xE4r man p\xE5visat v\xE4rde och f\xE5tt med fler p\xE5 t\xE5get.\n            SenseViews syfte \xE4r att underl\xE4tta och visuellt visa upp de p\xE5g\xE5ende pilotprojekt som en stad genomf\xF6r.',
        contact: 'kontakt',
        email_us: 'skicka epost till oss',
        email: 'epost',
        first_name: 'förnamn',
        fixed: 'åtgärdat',
        help: 'hjälp',
        language: 'språk',
        last_name: 'efternamn',
        log_out: 'logga ut',
        logged_in_as: 'inloggad som',
        new: 'nytt',
        reset_password_info: 'när du klickar på knappen skickas ett epost till följande adress med instruktioner',
        sent: 'skickat',
        to_dashboard: 'till Dashboard',
        updated: 'uppdaterat',
        username: 'användarnamn'
    },
    "en-US": {
        a_tool_by_allbin: 'a tool by All Binary',
        account_information: 'account information',
        change_password: 'change password',
        changed: 'changed',
        changelog: 'change log',
        company_info_1: 'All Binary operates within two primary business units with the common denominator that we make\n            it easy to handle, visualize and use data.',
        company_info_2: 'Our Transport business unit is focused on the increasing need to travel together using public\n            transit. We make analysis and planning of public transit easier by visualizing data from realtime and ticket\n            systems. We also create tools for keeping track of bus stops and work order management.',
        company_info_3: 'Our IOE business unit with the product SenseView is focused on helping cities and companies\n            become smarter. We believe in the power of experimenting with different sensors and pilots before investing\n            in bigger systems. It is important to get more people onboard and with SenseView it is easier to show what is ongoing in the smart city or industry.',
        contact: 'contact',
        email_us: 'email us',
        email: 'email',
        first_name: 'first name',
        fixed: 'fixed',
        help: 'help',
        language: 'language',
        last_name: 'last name',
        log_out: 'log out',
        logged_in_as: 'logged in as',
        new: 'new',
        reset_password_info: 'when you click the button an email will be sent to the follow address with instructions',
        sent: 'sent',
        to_dashboard: 'to Dashboard',
        updated: 'updated',
        username: 'username'
    }
};

exports.default = translations;