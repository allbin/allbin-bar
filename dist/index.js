'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(['\n            from {\n              opacity: 1;\n              transform: none;\n              visibility: visible;\n            }\n            to {\n              opacity: 0;\n              transform: translateX(-200px);\n              visibility: visible;\n            }\n        '], ['\n            from {\n              opacity: 1;\n              transform: none;\n              visibility: visible;\n            }\n            to {\n              opacity: 0;\n              transform: translateX(-200px);\n              visibility: visible;\n            }\n        ']),
    _templateObject2 = _taggedTemplateLiteral(['\n            from {\n              opacity: 0;\n              transform: translateX(-200px);\n            }\n            to {\n              opacity: 1;\n              transform: none;\n            }\n        '], ['\n            from {\n              opacity: 0;\n              transform: translateX(-200px);\n            }\n            to {\n              opacity: 1;\n              transform: none;\n            }\n        ']),
    _templateObject3 = _taggedTemplateLiteral(['\n            from {\n              opacity: 0;\n              transform: translateX(-10%);\n            }\n            to {\n              opacity: 1;\n              transform: none;\n            }\n        '], ['\n            from {\n              opacity: 0;\n              transform: translateX(-10%);\n            }\n            to {\n              opacity: 1;\n              transform: none;\n            }\n        ']),
    _templateObject4 = _taggedTemplateLiteral(['\n      z-index: 120;\n      bottom: 0;\n      color: #333;\n      left: 0;\n      right: 0;\n      top: 0;\n      bottom: 0;\n      pointer-events: none;\n      @media (max-width: 768px) {\n        .service_menu_list ul li .service_menu_list_label {\n          font-size: 20px !important;\n        }\n        .service_menu_actions a {\n          font-size: 20px !important;\n          padding: 18px 0 !important;\n        }\n        .service_menu .service_menu_title {\n          font-size: 24px !important;\n        }\n      }\n      .navbar {\n        pointer-events: all;\n        width: 100%;\n        background-color: ', ';\n        border-bottom: 1px solid\n          ', ';\n      }\n      &.render_menu {\n        .service_circle {\n          position: fixed;\n          padding: 9px;\n          height: 60px;\n        }\n        .styled_service_name {\n          position: absolute;\n          left: 272px;\n          top: 8px;\n          background-color: rgba(0, 0, 0, 0.2);\n          z-index: 109;\n          width: 48px;\n          height: 48px;\n          border-radius: 200px 0 0 200px;\n          text-align: center;\n          padding: 14px 0;\n          color: #fff;\n          &:hover {\n            background-color: rgba(0, 0, 0, 0.4);\n          }\n        }\n        @media (max-width: 768px) {\n          .styled_service_name {\n            left: unset;\n            right: 0;\n          }\n        }\n        .navbar {\n          background-color: transparent;\n          border: none;\n          box-shadow: none;\n          position: relative;\n        }\n      }\n      &:not(.render_menu) {\n        .service_menu_list_content {\n          visibility: hidden;\n        }\n      }\n\n      &.menu_option_expanded {\n        width: ', ';\n\n        .service_control {\n          width: ', ';\n        }\n        .service_menu {\n          width: ', ';\n        }\n        .service_menu_list {\n          top: 0;\n        }\n        .service_menu_list_content {\n          visibility: visible;\n          pointer-events: all;\n          opacity: 1;\n          transition: all 0.4s 0.2s;\n        }\n        .service_menu_actions,\n        .service_design {\n          display: none;\n        }\n        .styled_service_name {\n          left: 6px;\n          top: 0px;\n          color: #000;\n          background-color: transparent;\n          margin: 0;\n          height: 56px;\n          opacity: 0.6;\n          &:hover {\n            opacity: 1;\n            background-color: transparent;\n          }\n          svg {\n            height: 28px;\n            width: 28px;\n          }\n        }\n      }\n\n      @media (max-width: 768px) {\n        &.menu_option_expanded {\n          width: 100%;\n          .service_control,\n          .service_menu {\n            width: 100%;\n          }\n        }\n      }\n\n      .service_menu_list_content {\n        background-color: #f5f6fb;\n        position: absolute;\n        left: 0;\n        right: 0;\n        top: 56px;\n        bottom: 0;\n        visibility: hidden;\n        pointer-events: none;\n        overflow: auto;\n        opacity: 0;\n        transition: none;\n      }\n      .service_menu_fade {\n        pointer-events: all;\n        background-color: rgba(81, 97, 132, 0.79);\n        position: fixed;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        z-index: 100;\n      }\n      .service_control {\n        z-index: 100;\n        position: fixed;\n        top: 0;\n        left: 0;\n        bottom: 0;\n        width: 320px;\n        &.hide {\n          display: none;\n        }\n        &.enabled {\n          pointer-events: all;\n          visibility: visible;\n          animation: ', ' 400ms 0s both;\n          .service_menu_list {\n            animation: ', ' 500ms 100ms both;\n          }\n        }\n        &.disabled {\n          pointer-events: none;\n          animation: ', ' 400ms 0s both;\n        }\n      }\n      @media (max-width: 768px) {\n        .service_control {\n          width: 100%;\n        }\n      }\n      .styled_service_name {\n        cursor: pointer;\n        pointer-events: all;\n        border-radius: 0 200px 200px 0;\n        background-color: rgba(0, 0, 0, 0.3);\n        padding: 14px 28px 14px 40px;\n        margin-top: 8px;\n        display: inline-block;\n        color: #fff;\n        font-weight: 700;\n        transition: all 0.3s;\n        &:hover {\n          background-color: rgba(0, 0, 0, 0.4);\n        }\n      }\n      .service_circle {\n        z-index: 104;\n        cursor: pointer;\n        color: ', ';\n        text-align: center;\n        transition: all 0.4s;\n        top: 0;\n        width: 50px;\n        height: ', ';\n        position: relative;\n        &:hover {\n          color: rgba(0, 0, 0, 0.2);\n        }\n        svg {\n          transform: scale(-1, 1);\n          height: ', ';\n          font-size: 26px;\n        }\n      }\n      .service_menu {\n        overflow: hidden;\n        z-index: 103;\n        box-shadow: 0 2px 14px 0 rgba(0, 0, 0, 0.14);\n        background-color: #f6f6fa;\n        color: #272c44;\n        width: 320px;\n        height: 100%;\n        transition: width 0.5s;\n\n        .service_menu_title {\n          cursor: pointer;\n          text-align: right;\n          padding: 20px;\n          font-size: 16px;\n          &:hover {\n            background-color: rgba(0, 0, 0, 0.08);\n          }\n          &.hide {\n            display: none;\n          }\n          .service_menu_title_label {\n            font-size: 12px;\n            opacity: 0.7;\n            margin-bottom: 4px;\n          }\n        }\n      }\n      @media (max-width: 768px) {\n        .service_menu {\n          width: 100%;\n        }\n      }\n      .service_design {\n        padding: 50px 26px;\n        color: #fff;\n        text-shadow: 0 2px 3px rgba(0, 0, 0, 0.05);\n        background-color: ', ';\n        .service_design_title {\n          font-size: 22px;\n          margin-bottom: 8px;\n        }\n        .service_design_subtitle {\n          opacity: 0.65;\n          font-size: 14px;\n        }\n      }\n      .service_menu_list {\n        font-size: 14px;\n        z-index: 110;\n        transition: all 0.5s;\n        ul {\n          font-size: 14px;\n          li {\n            cursor: pointer;\n            background-color: #fff;\n            border-bottom: 1px solid #e7eaf1;\n            &:hover {\n              background-color: #e7eaf1;\n            }\n            &.hide {\n              display: none;\n            }\n            &.title_big {\n              margin: 0;\n              border-radius: 0;\n              font-size: 22px;\n              cursor: unset;\n              box-shadow: none;\n              &:hover {\n                background-color: transparent;\n              }\n              .service_menu_list_icon {\n                opacity: 0;\n              }\n              .service_menu_list_label {\n                padding: 16px 0px 16px 0px;\n              }\n              .service_menu_list_icon {\n                width: 60px;\n                .service_menu_list_icon_dot {\n                }\n              }\n            }\n            .service_menu_list_icon,\n            .service_menu_list_label {\n              display: inline-block;\n              vertical-align: middle;\n            }\n            .service_menu_list_label {\n              padding: 20px 20px;\n            }\n            .service_menu_list_icon {\n              width: 40px;\n              text-align: center;\n              padding: 13px 22px;\n              .service_menu_list_icon_dot {\n                height: 24px;\n                width: 24px;\n                border-radius: 50%;\n                margin: auto;\n              }\n            }\n          }\n        }\n      }\n      .service_menu_actions {\n        background-color: #e7eaf1;\n        position: absolute;\n        left: 0;\n        bottom: 0;\n        right: 0;\n        padding-bottom: 8px;\n        button,\n        a {\n          font-size: 14px;\n          cursor: pointer;\n          border: none;\n          text-align: center;\n          padding: 16px 0;\n          color: #fff;\n          display: block;\n          border-radius: 3px;\n          margin: 8px 20px;\n          &.blue {\n            background-color: #0f94e0;\n            &:hover {\n              background-image: linear-gradient(\n                -90deg,\n                #0f94e0 0%,\n                #017abf 100%\n              );\n            }\n          }\n          &.black {\n            background-color: #fc6b4c;\n            &:hover {\n              background-color: #e05132;\n            }\n          }\n        }\n      }\n      .service_panel {\n        z-index: 100;\n        position: fixed;\n        top: 0;\n        right: 0;\n        bottom: 0;\n        background-color: #f7f9fc;\n        width: 580px;\n        border-left: 1px solid #dedede;\n        display: none;\n        &.open {\n          display: block;\n          animation: ', ' 500ms 100ms both;\n        }\n        .service_panel_title {\n          padding: 20px;\n          font-size: 38px;\n        }\n      }\n\n      .list_content_cover_image {\n        height: 320px;\n        background-image: url(https://www.allbinary.se/wp-content/uploads/2018/01/All-Binary-2000px-8.jpg);\n        background-size: cover;\n        background-position: center;\n      }\n      @media (max-width: 768px) {\n        .list_content_cover_image {\n          width: 100%;\n        }\n      }\n\n      .service_button_label {\n        font-size: 12px;\n        opacity: 0.6;\n      }\n\n      .list_content_text {\n        padding: 20px;\n        font-size: 14px;\n        .list_content_text_title {\n          font-size: 18px;\n          margin-bottom: 12px;\n          font-weight: bold;\n          opacity: 1;\n        }\n        .text_block {\n          opacity: 0.6;\n          margin: 20px 0;\n          font-size: 14px;\n          line-height: 18px;\n          p {\n            margin-bottom: 8px;\n          }\n        }\n        a {\n          color: #00a0e5;\n          font-weight: bold;\n          &:hover {\n            color: #008bc7;\n          }\n          &.block_style {\n            margin: 32px 0;\n          }\n        }\n        .data_block {\n          margin: 18px 0;\n          padding-bottom: 12px;\n          border-bottom: 2px solid rgba(15, 15, 15, 0.2);\n          &.u--no_border {\n            border-bottom: none;\n            margin-bottom: 32px;\n          }\n          .data_label {\n            opacity: 0.5;\n            margin-bottom: 8px;\n            font-size: 10px;\n            text-transform: uppercase;\n            font-weight: bold;\n          }\n          .data_content {\n            font-size: 14px;\n          }\n          .data_switch {\n            border-radius: 4px;\n            background-color: #00000024;\n            color: #fff;\n            padding: 4px;\n            text-align: center;\n            .data_switch_option {\n              cursor: pointer;\n              border-radius: 4px;\n              display: inline-block;\n              vertical-align: middle;\n              width: 50%;\n              padding: 12px 0;\n              &:first-child {\n                width: 49%;\n                margin-right: 1%;\n              }\n              &:hover {\n                background-color: rgba(0, 0, 0, 0.1);\n              }\n              &.active {\n                background-color: #3d87f5;\n                color: #fff;\n              }\n            }\n          }\n        }\n      }\n\n      .service_button {\n        border-radius: 3px;\n        cursor: pointer;\n        text-align: center;\n        display: block;\n        margin: 28px 0;\n        padding: 18px 0;\n        width: 100%;\n        color: #fff !important;\n        background-color: #3e87f5;\n        font-weight: bold;\n        &.checked {\n          background-color: #7ba9ed;\n          pointer-events: none;\n        }\n        &:hover {\n          background-color: #246bd5;\n        }\n      }\n\n      .service_menu_list_content_changelog {\n        .changelog_item {\n          margin: 12px 0;\n          padding: 20px 0;\n          &:nth-child(even) {\n            background-color: #eff1f6;\n            border-top: 2px solid #e7eaf1;\n            border-bottom: 2px solid #e7eaf1;\n          }\n        }\n        .changelog_date_block,\n        .changelog_items_block {\n          display: inline-block;\n          vertical-align: top;\n        }\n        .changelog_date_block {\n          width: 112px;\n          text-align: right;\n          font-size: 12px;\n          opacity: 0.7;\n          padding-top: 4px;\n        }\n        .changelog_item_label {\n          padding: 4px 0px;\n          font-weight: bold;\n          width: 82px;\n          border-radius: 4px;\n          color: #fff;\n          background-color: gray;\n          display: inline-block;\n          font-size: 12px;\n          text-align: center;\n          &.new {\n            background-color: #61c02e;\n          }\n          &.fixed {\n            background-color: #ed9b6b;\n          }\n          &.updated {\n            background-color: #00a0e5;\n          }\n        }\n        .changelog_items_block {\n          padding: 0 0px 0px 24px;\n          margin-bottom: 12px;\n          .changelog_item_title {\n            font-size: 18px;\n            font-weight: bold;\n            margin-bottom: 12px;\n          }\n          .changelog_item_list {\n            margin-bottom: 20px;\n            &:last-child {\n              margin-bottom: 0;\n            }\n            ul {\n              padding-left: 8px;\n              border-left: 2px solid gray;\n            }\n          }\n\n          ul {\n            padding-top: 8px;\n            padding-bottom: 4px;\n            font-size: 14px;\n            opacity: 0.8;\n            li {\n              margin-bottom: 8px;\n              &:before {\n                content: \'- \';\n              }\n              &:last-child {\n                margin-bottom: 0;\n              }\n            }\n          }\n        }\n      }\n      b {\n        font-weight: bold;\n      }\n    '], ['\n      z-index: 120;\n      bottom: 0;\n      color: #333;\n      left: 0;\n      right: 0;\n      top: 0;\n      bottom: 0;\n      pointer-events: none;\n      @media (max-width: 768px) {\n        .service_menu_list ul li .service_menu_list_label {\n          font-size: 20px !important;\n        }\n        .service_menu_actions a {\n          font-size: 20px !important;\n          padding: 18px 0 !important;\n        }\n        .service_menu .service_menu_title {\n          font-size: 24px !important;\n        }\n      }\n      .navbar {\n        pointer-events: all;\n        width: 100%;\n        background-color: ', ';\n        border-bottom: 1px solid\n          ', ';\n      }\n      &.render_menu {\n        .service_circle {\n          position: fixed;\n          padding: 9px;\n          height: 60px;\n        }\n        .styled_service_name {\n          position: absolute;\n          left: 272px;\n          top: 8px;\n          background-color: rgba(0, 0, 0, 0.2);\n          z-index: 109;\n          width: 48px;\n          height: 48px;\n          border-radius: 200px 0 0 200px;\n          text-align: center;\n          padding: 14px 0;\n          color: #fff;\n          &:hover {\n            background-color: rgba(0, 0, 0, 0.4);\n          }\n        }\n        @media (max-width: 768px) {\n          .styled_service_name {\n            left: unset;\n            right: 0;\n          }\n        }\n        .navbar {\n          background-color: transparent;\n          border: none;\n          box-shadow: none;\n          position: relative;\n        }\n      }\n      &:not(.render_menu) {\n        .service_menu_list_content {\n          visibility: hidden;\n        }\n      }\n\n      &.menu_option_expanded {\n        width: ', ';\n\n        .service_control {\n          width: ', ';\n        }\n        .service_menu {\n          width: ', ';\n        }\n        .service_menu_list {\n          top: 0;\n        }\n        .service_menu_list_content {\n          visibility: visible;\n          pointer-events: all;\n          opacity: 1;\n          transition: all 0.4s 0.2s;\n        }\n        .service_menu_actions,\n        .service_design {\n          display: none;\n        }\n        .styled_service_name {\n          left: 6px;\n          top: 0px;\n          color: #000;\n          background-color: transparent;\n          margin: 0;\n          height: 56px;\n          opacity: 0.6;\n          &:hover {\n            opacity: 1;\n            background-color: transparent;\n          }\n          svg {\n            height: 28px;\n            width: 28px;\n          }\n        }\n      }\n\n      @media (max-width: 768px) {\n        &.menu_option_expanded {\n          width: 100%;\n          .service_control,\n          .service_menu {\n            width: 100%;\n          }\n        }\n      }\n\n      .service_menu_list_content {\n        background-color: #f5f6fb;\n        position: absolute;\n        left: 0;\n        right: 0;\n        top: 56px;\n        bottom: 0;\n        visibility: hidden;\n        pointer-events: none;\n        overflow: auto;\n        opacity: 0;\n        transition: none;\n      }\n      .service_menu_fade {\n        pointer-events: all;\n        background-color: rgba(81, 97, 132, 0.79);\n        position: fixed;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        z-index: 100;\n      }\n      .service_control {\n        z-index: 100;\n        position: fixed;\n        top: 0;\n        left: 0;\n        bottom: 0;\n        width: 320px;\n        &.hide {\n          display: none;\n        }\n        &.enabled {\n          pointer-events: all;\n          visibility: visible;\n          animation: ', ' 400ms 0s both;\n          .service_menu_list {\n            animation: ', ' 500ms 100ms both;\n          }\n        }\n        &.disabled {\n          pointer-events: none;\n          animation: ', ' 400ms 0s both;\n        }\n      }\n      @media (max-width: 768px) {\n        .service_control {\n          width: 100%;\n        }\n      }\n      .styled_service_name {\n        cursor: pointer;\n        pointer-events: all;\n        border-radius: 0 200px 200px 0;\n        background-color: rgba(0, 0, 0, 0.3);\n        padding: 14px 28px 14px 40px;\n        margin-top: 8px;\n        display: inline-block;\n        color: #fff;\n        font-weight: 700;\n        transition: all 0.3s;\n        &:hover {\n          background-color: rgba(0, 0, 0, 0.4);\n        }\n      }\n      .service_circle {\n        z-index: 104;\n        cursor: pointer;\n        color: ', ';\n        text-align: center;\n        transition: all 0.4s;\n        top: 0;\n        width: 50px;\n        height: ', ';\n        position: relative;\n        &:hover {\n          color: rgba(0, 0, 0, 0.2);\n        }\n        svg {\n          transform: scale(-1, 1);\n          height: ', ';\n          font-size: 26px;\n        }\n      }\n      .service_menu {\n        overflow: hidden;\n        z-index: 103;\n        box-shadow: 0 2px 14px 0 rgba(0, 0, 0, 0.14);\n        background-color: #f6f6fa;\n        color: #272c44;\n        width: 320px;\n        height: 100%;\n        transition: width 0.5s;\n\n        .service_menu_title {\n          cursor: pointer;\n          text-align: right;\n          padding: 20px;\n          font-size: 16px;\n          &:hover {\n            background-color: rgba(0, 0, 0, 0.08);\n          }\n          &.hide {\n            display: none;\n          }\n          .service_menu_title_label {\n            font-size: 12px;\n            opacity: 0.7;\n            margin-bottom: 4px;\n          }\n        }\n      }\n      @media (max-width: 768px) {\n        .service_menu {\n          width: 100%;\n        }\n      }\n      .service_design {\n        padding: 50px 26px;\n        color: #fff;\n        text-shadow: 0 2px 3px rgba(0, 0, 0, 0.05);\n        background-color: ', ';\n        .service_design_title {\n          font-size: 22px;\n          margin-bottom: 8px;\n        }\n        .service_design_subtitle {\n          opacity: 0.65;\n          font-size: 14px;\n        }\n      }\n      .service_menu_list {\n        font-size: 14px;\n        z-index: 110;\n        transition: all 0.5s;\n        ul {\n          font-size: 14px;\n          li {\n            cursor: pointer;\n            background-color: #fff;\n            border-bottom: 1px solid #e7eaf1;\n            &:hover {\n              background-color: #e7eaf1;\n            }\n            &.hide {\n              display: none;\n            }\n            &.title_big {\n              margin: 0;\n              border-radius: 0;\n              font-size: 22px;\n              cursor: unset;\n              box-shadow: none;\n              &:hover {\n                background-color: transparent;\n              }\n              .service_menu_list_icon {\n                opacity: 0;\n              }\n              .service_menu_list_label {\n                padding: 16px 0px 16px 0px;\n              }\n              .service_menu_list_icon {\n                width: 60px;\n                .service_menu_list_icon_dot {\n                }\n              }\n            }\n            .service_menu_list_icon,\n            .service_menu_list_label {\n              display: inline-block;\n              vertical-align: middle;\n            }\n            .service_menu_list_label {\n              padding: 20px 20px;\n            }\n            .service_menu_list_icon {\n              width: 40px;\n              text-align: center;\n              padding: 13px 22px;\n              .service_menu_list_icon_dot {\n                height: 24px;\n                width: 24px;\n                border-radius: 50%;\n                margin: auto;\n              }\n            }\n          }\n        }\n      }\n      .service_menu_actions {\n        background-color: #e7eaf1;\n        position: absolute;\n        left: 0;\n        bottom: 0;\n        right: 0;\n        padding-bottom: 8px;\n        button,\n        a {\n          font-size: 14px;\n          cursor: pointer;\n          border: none;\n          text-align: center;\n          padding: 16px 0;\n          color: #fff;\n          display: block;\n          border-radius: 3px;\n          margin: 8px 20px;\n          &.blue {\n            background-color: #0f94e0;\n            &:hover {\n              background-image: linear-gradient(\n                -90deg,\n                #0f94e0 0%,\n                #017abf 100%\n              );\n            }\n          }\n          &.black {\n            background-color: #fc6b4c;\n            &:hover {\n              background-color: #e05132;\n            }\n          }\n        }\n      }\n      .service_panel {\n        z-index: 100;\n        position: fixed;\n        top: 0;\n        right: 0;\n        bottom: 0;\n        background-color: #f7f9fc;\n        width: 580px;\n        border-left: 1px solid #dedede;\n        display: none;\n        &.open {\n          display: block;\n          animation: ', ' 500ms 100ms both;\n        }\n        .service_panel_title {\n          padding: 20px;\n          font-size: 38px;\n        }\n      }\n\n      .list_content_cover_image {\n        height: 320px;\n        background-image: url(https://www.allbinary.se/wp-content/uploads/2018/01/All-Binary-2000px-8.jpg);\n        background-size: cover;\n        background-position: center;\n      }\n      @media (max-width: 768px) {\n        .list_content_cover_image {\n          width: 100%;\n        }\n      }\n\n      .service_button_label {\n        font-size: 12px;\n        opacity: 0.6;\n      }\n\n      .list_content_text {\n        padding: 20px;\n        font-size: 14px;\n        .list_content_text_title {\n          font-size: 18px;\n          margin-bottom: 12px;\n          font-weight: bold;\n          opacity: 1;\n        }\n        .text_block {\n          opacity: 0.6;\n          margin: 20px 0;\n          font-size: 14px;\n          line-height: 18px;\n          p {\n            margin-bottom: 8px;\n          }\n        }\n        a {\n          color: #00a0e5;\n          font-weight: bold;\n          &:hover {\n            color: #008bc7;\n          }\n          &.block_style {\n            margin: 32px 0;\n          }\n        }\n        .data_block {\n          margin: 18px 0;\n          padding-bottom: 12px;\n          border-bottom: 2px solid rgba(15, 15, 15, 0.2);\n          &.u--no_border {\n            border-bottom: none;\n            margin-bottom: 32px;\n          }\n          .data_label {\n            opacity: 0.5;\n            margin-bottom: 8px;\n            font-size: 10px;\n            text-transform: uppercase;\n            font-weight: bold;\n          }\n          .data_content {\n            font-size: 14px;\n          }\n          .data_switch {\n            border-radius: 4px;\n            background-color: #00000024;\n            color: #fff;\n            padding: 4px;\n            text-align: center;\n            .data_switch_option {\n              cursor: pointer;\n              border-radius: 4px;\n              display: inline-block;\n              vertical-align: middle;\n              width: 50%;\n              padding: 12px 0;\n              &:first-child {\n                width: 49%;\n                margin-right: 1%;\n              }\n              &:hover {\n                background-color: rgba(0, 0, 0, 0.1);\n              }\n              &.active {\n                background-color: #3d87f5;\n                color: #fff;\n              }\n            }\n          }\n        }\n      }\n\n      .service_button {\n        border-radius: 3px;\n        cursor: pointer;\n        text-align: center;\n        display: block;\n        margin: 28px 0;\n        padding: 18px 0;\n        width: 100%;\n        color: #fff !important;\n        background-color: #3e87f5;\n        font-weight: bold;\n        &.checked {\n          background-color: #7ba9ed;\n          pointer-events: none;\n        }\n        &:hover {\n          background-color: #246bd5;\n        }\n      }\n\n      .service_menu_list_content_changelog {\n        .changelog_item {\n          margin: 12px 0;\n          padding: 20px 0;\n          &:nth-child(even) {\n            background-color: #eff1f6;\n            border-top: 2px solid #e7eaf1;\n            border-bottom: 2px solid #e7eaf1;\n          }\n        }\n        .changelog_date_block,\n        .changelog_items_block {\n          display: inline-block;\n          vertical-align: top;\n        }\n        .changelog_date_block {\n          width: 112px;\n          text-align: right;\n          font-size: 12px;\n          opacity: 0.7;\n          padding-top: 4px;\n        }\n        .changelog_item_label {\n          padding: 4px 0px;\n          font-weight: bold;\n          width: 82px;\n          border-radius: 4px;\n          color: #fff;\n          background-color: gray;\n          display: inline-block;\n          font-size: 12px;\n          text-align: center;\n          &.new {\n            background-color: #61c02e;\n          }\n          &.fixed {\n            background-color: #ed9b6b;\n          }\n          &.updated {\n            background-color: #00a0e5;\n          }\n        }\n        .changelog_items_block {\n          padding: 0 0px 0px 24px;\n          margin-bottom: 12px;\n          .changelog_item_title {\n            font-size: 18px;\n            font-weight: bold;\n            margin-bottom: 12px;\n          }\n          .changelog_item_list {\n            margin-bottom: 20px;\n            &:last-child {\n              margin-bottom: 0;\n            }\n            ul {\n              padding-left: 8px;\n              border-left: 2px solid gray;\n            }\n          }\n\n          ul {\n            padding-top: 8px;\n            padding-bottom: 4px;\n            font-size: 14px;\n            opacity: 0.8;\n            li {\n              margin-bottom: 8px;\n              &:before {\n                content: \'- \';\n              }\n              &:last-child {\n                margin-bottom: 0;\n              }\n            }\n          }\n        }\n      }\n      b {\n        font-weight: bold;\n      }\n    ']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _outputHelpers = require('output-helpers');

var _outputHelpers2 = _interopRequireDefault(_outputHelpers);

var _translations = require('./translations');

var _translations2 = _interopRequireDefault(_translations);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IconMenu = require('react-icons/lib/md/menu');
var IconRemove = require('react-icons/lib/md/close');
var IconChangelog = require('react-icons/lib/md/playlist-add-check');
var IconHelp = require('react-icons/lib/md/help');
var IconContact = require('react-icons/lib/md/mail');
var IconBack = require('react-icons/lib/md/arrow-back');

_outputHelpers2.default.addDictionary(_translations2.default);

var ServiceController = function (_React$Component) {
  _inherits(ServiceController, _React$Component);

  function ServiceController(props) {
    _classCallCheck(this, ServiceController);

    var _this = _possibleConstructorReturn(this, (ServiceController.__proto__ || Object.getPrototypeOf(ServiceController)).call(this, props));

    _this.state = {
      render_menu: false,
      first_triggered: false,
      render_menu_option: -1,
      reset_password_clicked: false,
      credentials_available: window.hasOwnProperty('sso') && window.sso.isLoggedIn()
    };

    var sizes = {
      w_menu_option_expanded: 600,
      h_navbar: 40
    };

    var fade_out = (0, _styledComponents.keyframes)(_templateObject);

    var fade_in = (0, _styledComponents.keyframes)(_templateObject2);

    var fade_in_right = (0, _styledComponents.keyframes)(_templateObject3);

    _this.Container = _styledComponents2.default.div(_templateObject4, _this.props.background ? _this.props.background : '#fff', function (props) {
      return _this.props.background ? 'transparent' : props.theme.colors.border;
    }, sizes.w_menu_option_expanded + 'px', sizes.w_menu_option_expanded + 'px', sizes.w_menu_option_expanded + 'px', fade_in, fade_in_right, fade_out, function (props) {
      return props.theme.colors.border;
    }, sizes.h_navbar + 'px', sizes.h_navbar + 'px', _this.props.background ? _this.props.background : '#647BFF', fade_in_right);
    return _this;
  }

  _createClass(ServiceController, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (!this.props.service_name) {
        console.warn("Missing expected prop 'service_name'!");
      }
    }
  }, {
    key: 'translate',
    value: function translate(str) {
      if (this.props.language) {
        return _outputHelpers2.default.translate(_translations2.default.prefix + str, undefined, this.props.language);
      }
      return _outputHelpers2.default.translate(_translations2.default.prefix + str);
    }
  }, {
    key: 'getTitleClasses',
    value: function getTitleClasses(index) {
      var class_name = 'title_big';
      if (this.state.render_menu_option === -1) {
        return '';
      }
      if (this.state.render_menu_option !== index) {
        class_name = 'hide';
      }
      return class_name;
    }
  }, {
    key: 'hoverContainerClick',
    value: function hoverContainerClick() {
      var render_menu = this.state.render_menu;
      var first_triggered = this.state.first_triggered;
      var render_menu_option = this.state.render_menu_option;

      if (!this.state.render_menu) {
        render_menu = true;
        first_triggered = true;
      }

      if (this.state.render_menu && this.state.render_menu_option === -1) {
        render_menu = false;
      }

      if (this.state.render_menu && this.state.render_menu_option !== -1) {
        render_menu_option = -1;
        render_menu = true;
      }

      this.setState({
        render_menu: render_menu,
        first_triggered: first_triggered,
        render_menu_option: render_menu_option
      });
    }
  }, {
    key: 'handleResetPassword',
    value: function handleResetPassword() {
      this.setState({
        reset_password_clicked: true
      });
    }
  }, {
    key: 'getLanguageFromObject',
    value: function getLanguageFromObject(obj) {
      var lang = this.props.langauge || _outputHelpers2.default.getLang();
      if (obj.hasOwnProperty(lang)) {
        return obj[lang];
      } else if (obj.hasOwnProperty(_outputHelpers2.default.getFallbackLang())) {
        return obj[_outputHelpers2.default.getFallbackLang()];
      }
      console.warn('OH: Changelog entry does not contain specified language or fallback language translation.');
      return '';
    }
  }, {
    key: 'getChangelogEntryTitle',
    value: function getChangelogEntryTitle(changelog_entry) {
      if (_typeof(changelog_entry.title) === 'object' && !Array.isArray(changelog_entry.title)) {
        return this.getLanguageFromObject(changelog_entry.title);
      }
      return changelog_entry.title;
    }
  }, {
    key: 'renderChangeLog',
    value: function renderChangeLog() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'service_menu_list_content_changelog' },
        this.props.changelog.map(function (change_entry, i) {
          var change_types = ['new', 'updated', 'fixed'];
          return _react2.default.createElement(
            'div',
            { className: 'changelog_item', key: i },
            _react2.default.createElement(
              'div',
              { className: 'changelog_date_block' },
              _react2.default.createElement(
                'p',
                null,
                change_entry.date
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'changelog_items_block' },
              _react2.default.createElement(
                'p',
                { className: 'changelog_item_title' },
                _this2.getChangelogEntryTitle(change_entry)
              )
            ),
            change_types.map(function (type, type_index) {
              if (change_entry[type] === undefined) {
                return null;
              }

              return _react2.default.createElement(
                'div',
                { key: type_index },
                _react2.default.createElement(
                  'div',
                  { className: 'changelog_date_block' },
                  _react2.default.createElement(
                    'div',
                    { className: 'changelog_item_label ' + type },
                    _this2.translate(type)
                  )
                ),
                _react2.default.createElement(
                  'div',
                  {
                    className: 'changelog_items_block changelog_item_list ' + type,
                    key: type_index
                  },
                  _react2.default.createElement(
                    'ul',
                    null,
                    change_entry[type].map(function (a_item, z) {
                      return _react2.default.createElement(
                        'li',
                        { key: z },
                        (typeof a_item === 'undefined' ? 'undefined' : _typeof(a_item)) === 'object' ? _this2.getLanguageFromObject(a_item) : a_item
                      );
                    })
                  )
                )
              );
            })
          );
        })
      );
    }
  }, {
    key: 'renderLanguageToggle',
    value: function renderLanguageToggle() {
      //Currently not supported.
      //TODO: Add support in all services for fetching language settings from profile API.
      // return (
      //     <div className="data_block u--no_border">
      //         <p className="data_label">
      //             { this.translate('language') }
      //         </p>
      //         <div className="data_switch">
      //             <div className="data_switch_option active">
      //                 Swedish
      //             </div>
      //             <div className="data_switch_option">
      //                 English
      //             </div>
      //         </div>
      //     </div>
      // );
      return null;
    }
  }, {
    key: 'renderPasswordResetBtns',
    value: function renderPasswordResetBtns() {
      var _this3 = this;

      if (!this.props.enable_password_reset) {
        return null;
      }

      return _react2.default.createElement(
        'div',
        { className: 'data_block u--no_border' },
        _react2.default.createElement(
          'div',
          {
            className: 'service_button ' + (this.state.reset_password_clicked ? 'checked' : ''),
            onClick: function onClick() {
              return _this3.handleResetPassword();
            }
          },
          this.state.reset_password_clicked ? this.translate('sent') : this.translate('change_password')
        ),
        _react2.default.createElement(
          'p',
          { className: 'service_button_label' },
          this.translate('reset_password_info'),
          ':',
          ' ',
          _react2.default.createElement(
            'b',
            null,
            window.sso.getJWT().getClaim('email')
          )
        )
      );
    }
  }, {
    key: 'renderContent',
    value: function renderContent() {
      var _this4 = this;

      if (this.state.render_menu_option === -1) {
        return null;
      }

      if (this.state.render_menu_option === 0) {
        return this.renderChangeLog();
      }

      if (this.state.render_menu_option === 2) {
        return _react2.default.createElement(
          'div',
          { className: 'service_menu_list_content_contact' },
          _react2.default.createElement('div', { className: 'list_content_cover_image' }),
          _react2.default.createElement(
            'div',
            { className: 'list_content_text' },
            _react2.default.createElement(
              'p',
              { className: 'list_content_text_title' },
              'All Binary'
            ),
            _react2.default.createElement(
              'p',
              { className: 'text_block' },
              _react2.default.createElement(
                'p',
                null,
                this.translate('company_info_1')
              ),
              _react2.default.createElement(
                'p',
                null,
                this.translate('company_info_2')
              ),
              _react2.default.createElement(
                'p',
                null,
                this.translate('company_info_3')
              )
            ),
            _react2.default.createElement(
              'a',
              {
                className: 'block_style',
                href: 'https://www.allbinary.se',
                target: '_blank',
                rel: 'noreferrer'
              },
              'https://www.allbinary.se'
            ),
            _react2.default.createElement(
              'a',
              {
                href: 'mailto:info@allbinary.se',
                className: 'service_button',
                target: '_blank',
                rel: 'noreferrer'
              },
              this.translate('email_us')
            )
          )
        );
      }

      if (this.state.render_menu_option === 99) {
        var claims = ['first_name', 'last_name', 'username', 'email'];
        var user_info = [];

        claims.forEach(function (claim, i) {
          user_info.push({
            label: _this4.translate(claim),
            data: window.sso.getJWT().getClaim(claim)
          });
        });

        return _react2.default.createElement(
          'div',
          { className: 'service_menu_list_content_contact' },
          _react2.default.createElement(
            'div',
            { className: 'list_content_text' },
            _react2.default.createElement(
              'p',
              { className: 'list_content_text_title' },
              this.translate('account_information')
            ),
            user_info.map(function (item, i) {
              return _react2.default.createElement(
                'div',
                { key: i, className: 'data_block' },
                _react2.default.createElement(
                  'p',
                  { className: 'data_label' },
                  item.label
                ),
                _react2.default.createElement(
                  'p',
                  { className: 'data_content' },
                  item.data
                )
              );
            }),
            this.renderLanguageToggle(),
            this.renderPasswordResetBtns()
          )
        );
      }
    }
  }, {
    key: 'handleLogout',
    value: function handleLogout() {
      window.sso.logout('https://login.allbin.se/');
    }
  }, {
    key: 'render',
    value: function render() {
      var _this5 = this;

      return _react2.default.createElement(
        this.Container,
        {
          className: (this.state.render_menu ? 'render_menu' : 'disabled') + ' ' + (this.state.render_menu_option !== -1 ? 'menu_option_expanded' : '')
        },
        _react2.default.createElement(
          'div',
          { className: this.props.navbar ? 'navbar' : '' },
          this.props.styled_service_name ? _react2.default.createElement(
            'div',
            {
              className: 'styled_service_name',
              onClick: function onClick() {
                return _this5.hoverContainerClick();
              }
            },
            this.state.render_menu ? _react2.default.createElement(IconBack, null) : this.props.service_name
          ) : _react2.default.createElement(
            'div',
            {
              className: 'service_circle',
              onClick: function onClick() {
                return _this5.hoverContainerClick();
              }
            },
            this.state.render_menu ? _react2.default.createElement(IconRemove, null) : _react2.default.createElement(IconMenu, null)
          )
        ),
        this.state.render_menu ? _react2.default.createElement('div', {
          className: 'service_menu_fade',
          onClick: function onClick() {
            return _this5.setState({ render_menu: false, render_menu_option: -1 });
          }
        }) : null,
        _react2.default.createElement(
          'div',
          {
            className: 'service_control ' + (this.state.render_menu ? 'enabled' : 'disabled') + ' ' + (!this.state.first_triggered ? 'hide' : '')
          },
          _react2.default.createElement(
            'div',
            { className: 'service_menu' },
            _react2.default.createElement(
              'div',
              { className: 'service_design' },
              _react2.default.createElement(
                'p',
                { className: 'service_design_title' },
                this.props.service_name
              ),
              _react2.default.createElement(
                'p',
                { className: 'service_design_subtitle' },
                this.translate('a_tool_by_allbin')
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'service_menu_list' },
              _react2.default.createElement(
                'ul',
                null,
                this.props.changelog ? _react2.default.createElement(
                  'li',
                  {
                    className: '' + this.getTitleClasses(0),
                    onClick: function onClick() {
                      return _this5.setState({ render_menu_option: 0 });
                    }
                  },
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_icon' },
                    _react2.default.createElement(IconChangelog, { className: 'service_menu_list_icon_dot' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_label' },
                    this.translate('changelog')
                  )
                ) : null,
                this.props.help ? _react2.default.createElement(
                  'li',
                  {
                    className: '' + this.getTitleClasses(1),
                    onClick: function onClick() {
                      return _this5.setState({ render_menu_option: 1 });
                    }
                  },
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_icon' },
                    _react2.default.createElement(IconHelp, { className: 'service_menu_list_icon_dot' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_label' },
                    this.translate('help')
                  )
                ) : null,
                this.props.contact ? _react2.default.createElement(
                  'li',
                  {
                    className: '' + this.getTitleClasses(2),
                    onClick: function onClick() {
                      return _this5.setState({ render_menu_option: 2 });
                    }
                  },
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_icon' },
                    _react2.default.createElement(IconContact, { className: 'service_menu_list_icon_dot' })
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'service_menu_list_label' },
                    this.translate('contact')
                  )
                ) : null
              )
            ),
            _react2.default.createElement(
              'div',
              { className: 'service_menu_list_content' },
              this.renderContent()
            ),
            _react2.default.createElement(
              'div',
              { className: 'service_menu_actions' },
              this.props.show_credentials && this.state.credentials_available ? _react2.default.createElement(
                'div',
                {
                  className: 'service_menu_title ' + (this.state.render_menu_option !== -1 ? 'hide' : ''),
                  onClick: function onClick() {
                    return _this5.setState({ render_menu_option: 99 });
                  }
                },
                _react2.default.createElement(
                  'p',
                  { className: 'service_menu_title_label' },
                  this.translate('logged_in_as')
                ),
                _react2.default.createElement(
                  'p',
                  { className: 'service_menu_title_data' },
                  window.sso.getJWT().getClaim('username')
                )
              ) : null,
              this.props.dashboard_link ? _react2.default.createElement(
                'a',
                { className: 'blue', href: 'https://dashboard.allbin.se' },
                this.translate('to_dashboard')
              ) : null,
              _react2.default.createElement(
                'a',
                { className: 'black', onClick: function onClick() {
                    return _this5.handleLogout();
                  } },
                this.translate('log_out')
              )
            )
          )
        )
      );
    }
  }]);

  return ServiceController;
}(_react2.default.Component);

exports.default = ServiceController;