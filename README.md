# allbin-bar

## Usage

`import ABBar from 'allbin-bar';`

```
<ABBar
    service_name: "My service",
    contact: true,
    show_credentials: true,
    language: "sv-SE"
/>
```

## Props
`service_name <string, expected>` - Will warn if not present but does not break the component.

`changelog <array of objects>` - Enables the changelog category.

>NOTE: `title`, `new`, `updated` and `fixed` arrays may contain an array of strings or an array of objects which has language props on them for translations.  
>NOTE: Strings or objects with language props can be freely mixed. Between and inside changelog entries.  
>NOTE: All values will be converted to strings if the value is not already a string.

```
changelog object:
{
    date: <string, converted to string otherwise>,
    title: <array of strings || array of objects>,
    new: <array of strings || array of objects, optional>,
    updated: <array of strings || array of objects, optional>,
    fixed: <array of strings || array of objects, optional>
}

examples:
{
    date: "2018-06-02",
    title: "1.0.2",
    fixed: ["Bug fixed where login was not remembered in certain circumstances"]
}

{
    date: "2018-06-01",
    title: {
        "sv-SE": "1.0.0 Första versionen ute!",
        "en-US": "1.0.0 Out of beta!"
    },
    new: [
        {
            "sv-SE": "Knapp för att logga ut i kontrollpanelen.",
            "en-US": "Logout button added to control panel."
        }
    ],
    updated: [
        {
            "sv-SE": "Bytt bakgrundsbild.",
            "en-US": "Changed background picture."
        }
    ],
}

```

`contact <true || false, default: false>` - Show or hides the contact category.

`enable_password_reset <true || false, default: false>` - Shows or hides the password reset button in the user account information view. **CURRENTLY NOT IMPLEMENTED!**

`help <true || false, default: false>` - Show or hides the help category. **CURRENTLY NOT IMPLEMENTED!**

`language <"sv-SE" || "en-US">` - Sets the language used for translations.
> NOTE: Defaults to whatever is set in `output-helpers.setConfig(...)`.

`show_credentials <true || false, default: false>` - When `true` shows *Logged in as [username]* when sso login credentials are available. The *Logged in as [username]* can be clicked to view the user account information.  
When `false` hides the *Logged in as [username]*, no matter if login credentials are available.

