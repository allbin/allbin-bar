import React from 'react';
import styled, { keyframes } from 'styled-components';
import oh from 'output-helpers';
import translations from './translations';
const IconMenu = require('react-icons/lib/md/menu');
const IconRemove = require('react-icons/lib/md/close');
const IconChangelog = require('react-icons/lib/md/playlist-add-check');
const IconHelp = require('react-icons/lib/md/help');
const IconContact = require('react-icons/lib/md/mail');
const IconBack = require('react-icons/lib/md/arrow-back');

oh.addDictionary(translations);

class ServiceController extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            render_menu: false,
            first_triggered: false,
            render_menu_option: -1,
            reset_password_clicked: false,
            credentials_available: window.hasOwnProperty('sso') && window.sso.isLoggedIn()
        };



        const sizes = {
            w_menu_option_expanded: 600,
            h_navbar: 40
        };

        const fade_out = keyframes`
            from {
              opacity: 1;
              transform: none;
              visibility: visible;
            }
            to {
              opacity: 0;
              transform: translateX(-200px);
              visibility: visible;
            }
        `;

        const fade_in = keyframes`
            from {
              opacity: 0;
              transform: translateX(-200px);
            }
            to {
              opacity: 1;
              transform: none;
            }
        `;

        const fade_in_right = keyframes`
            from {
              opacity: 0;
              transform: translateX(-10%);
            }
            to {
              opacity: 1;
              transform: none;
            }
        `;

        this.Container = styled.div`
            z-index: 120;
            bottom: 0;
            color: #333;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            pointer-events: none;
            @media (max-width: 768px) {
                .service_menu_list ul li .service_menu_list_label {
                    font-size: 20px !important;
                }
                .service_menu_actions a {
                    font-size: 20px !important;
                    padding: 18px 0 !important;
                }
                .service_menu .service_menu_title{
                    font-size: 24px !important;
                }
            }
            .navbar {
                pointer-events: all;
                width: 100%;
                background-color: ${this.props.background ? this.props.background : '#fff'};
                border-bottom: 1px solid ${props => this.props.background ? 'transparent' : props.theme.colors.border};
            }
            &.render_menu {
                .service_circle {
                    position: fixed;
                    padding: 9px;
                    height: 60px;
                }
                .styled_service_name{
                    position: absolute;
                    left: 272px;
                    top: 8px;
                    background-color: rgba(0,0,0,0.2);
                    z-index: 109;
                    width: 48px;
                    height: 48px;
                    border-radius: 200px 0 0 200px;
                    text-align: center;
                    padding: 14px 0;
                    color: #fff;
                    &:HOVER{
                        background-color: rgba(0,0,0,0.4);
                    }
                }
                .navbar {
                    background-color: transparent;
                    border: none;
                    box-shadow: none;
                    position: relative;
                }
            }
            &:not(.render_menu) {
                .service_menu_list_content {
                    visibility: hidden;
                }
            }

            &.menu_option_expanded {
                width: ${sizes.w_menu_option_expanded + 'px'};
                @media (max-width: 768px) {
                    width: 100%;
                }
                .service_control {
                    width: ${sizes.w_menu_option_expanded + 'px'};
                    @media (max-width: 768px) {
                        width: 100%;
                    }
                }
                .service_menu {
                    width: ${sizes.w_menu_option_expanded + 'px'};
                    @media (max-width: 768px) {
                        width: 100%;
                    }
                }
                .service_menu_list {
                    top: 0;
                }
                .service_menu_list_content {
                    visibility: visible;
                    pointer-events: all;
                    opacity: 1;
                    transition: all 0.4s 0.2s;
                }
                .service_menu_actions, .service_design {
                    display: none;
                }
                .styled_service_name{
                    left: 6px;
                    top: 0px;
                    color: #000;
                    background-color: transparent;
                    margin: 0;
                    height: 56px;
                    opacity: 0.6;
                    &:HOVER{
                        opacity: 1;
                        background-color: transparent;
                    }
                    svg{
                        height: 28px;
                        width: 28px;
                    }
                }
            }

            .service_menu_list_content {
                background-color: #F5F6FB;
                position: absolute;
                left: 0;
                right: 0;
                top: 56px;
                bottom: 0;
                visibility: hidden;
                pointer-events: none;
                overflow: auto;
                opacity: 0;
                transition: none;
            }
            .service_menu_fade {
                pointer-events: all;
                background-color: rgba(81, 97, 132, 0.79);
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                z-index: 100;
            }
            .service_control {
                z-index: 100;
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                width: 320px;
                @media (max-width: 768px) {
                    width: 100%;
                }
                &.hide {
                    display: none;
                }
                &.enabled {
                    pointer-events: all;
                    visibility: visible;
                    animation: ${fade_in} 400ms 0s both;
                    .service_menu_list {
                        animation: ${fade_in_right} 500ms 100ms both;
                    }
                }
                &.disabled {
                    pointer-events: none;
                    animation: ${fade_out} 400ms 0s both;
                }
            }
            .styled_service_name{
                cursor: pointer;
                pointer-events: all;
                border-radius: 0 200px 200px 0;
                background-color: rgba(0,0,0,0.3);
                padding: 14px 28px 14px 40px;
                margin-top: 8px;
                display: inline-block;
                color: #fff;
                font-weight: 700;
                transition: all 0.3s;
                &:HOVER{
                    background-color: rgba(0,0,0,0.4);
                }
            }
            .service_circle {
                z-index: 104;
                cursor: pointer;
                color: ${props => props.theme.colors.border};
                text-align: center;
                transition: all 0.4s;
                top: 0;
                width: 50px;
                height: ${sizes.h_navbar + 'px'};
                position: relative;
                &:HOVER {
                    color: rgba(0,0,0,0.2);
                }
                svg {
                    transform: scale(-1, 1);
                    height: ${sizes.h_navbar + 'px'};;
                    font-size: 26px;
                }
            }
            .service_menu {
                overflow: hidden;
                z-index: 103;
                box-shadow: 0 2px 14px 0 rgba(0,0,0,0.14);
                background-color: #F6F6FA;
                color: #272C44;
                width: 320px;
                height: 100%;
                transition: width 0.5s;
                @media (max-width: 768px) {
                    width: 100%;
                }
                .service_menu_title {
                    cursor: pointer;
                    text-align: right;
                    padding: 20px;
                    font-size: 16px;
                    &:HOVER {
                        background-color: rgba(0,0,0,0.08);
                    }
                    &.hide {
                        display: none;
                    }
                    .service_menu_title_label {
                        font-size: 12px;
                        opacity: 0.7;
                        margin-bottom: 4px;
                    }
                }
            }
            .service_design {
                padding: 50px 26px;
                color: #fff;
                text-shadow: 0 2px 3px rgba(0,0,0,0.05);
                background-color: ${this.props.background ? this.props.background : '#647BFF'};
                .service_design_title {
                    font-size: 22px;
                    margin-bottom: 8px;
                }
                .service_design_subtitle {
                    opacity: 0.65;
                    font-size: 14px;
                }
            }
            .service_menu_list {
                font-size: 14px;
                z-index: 110;
                transition: all 0.5s;
                ul {
                    font-size: 14px;
                    li {
                        cursor: pointer;
                        background-color: #fff;
                        border-bottom: 1px solid #e7eaf1;
                        &:HOVER {
                            background-color: #e7eaf1;
                        }
                        &.hide {
                            display: none;
                        }
                        &.title_big {
                            margin: 0;
                            border-radius: 0;
                            font-size: 22px;
                            cursor: unset;
                            box-shadow: none;
                            &:HOVER {
                                background-color: transparent;
                            }
                            .service_menu_list_icon {
                                opacity: 0;
                            }
                            .service_menu_list_label {
                                padding: 16px 0px 16px 0px;
                            }
                            .service_menu_list_icon {
                                width: 60px;
                                .service_menu_list_icon_dot {

                                }
                            }
                        }
                        .service_menu_list_icon, .service_menu_list_label {
                            display: inline-block;
                            vertical-align: middle;
                        }
                        .service_menu_list_label {
                            padding: 20px 20px;
                        }
                        .service_menu_list_icon {
                            width: 40px;
                            text-align: center;
                            padding: 13px 22px;
                            .service_menu_list_icon_dot {
                                height: 24px;
                                width: 24px;
                                border-radius: 50%;
                                margin: auto;
                            }
                        }
                    }
                }
            }
            .service_menu_actions {
                background-color: #E7EAF1;
                position: absolute;
                left: 0;
                bottom: 0;
                right: 0;
                padding-bottom: 8px;
                button, a {
                    font-size: 14px;
                    cursor: pointer;
                    border: none;
                    text-align: center;
                    padding: 16px 0;
                    color: #fff;
                    display: block;
                    border-radius: 3px;
                    margin: 8px 20px;
                    &.blue {
                        background-color: #0F94E0;
                        &:HOVER{
                            background-image: linear-gradient(-90deg, #0F94E0 0%, #017abf 100%);
                        }
                    }
                    &.black {
                        background-color: #FC6B4C;
                        &:HOVER {
                            background-color: #e05132;
                        }
                    }
                }
            }
            .service_panel {
                z-index: 100;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                background-color: #F7F9FC;
                width: 580px;
                border-left: 1px solid #DEDEDE;
                display: none;
                &.open {
                    display: block;
                    animation: ${fade_in_right} 500ms 100ms both;
                }
                .service_panel_title {
                    padding: 20px;
                    font-size: 38px;
                }
            }

            .list_content_cover_image {
                height: 320px;
                background-image: url(https://www.allbinary.se/wp-content/uploads/2018/01/All-Binary-2000px-8.jpg);
                background-size: cover;
                background-position: center;
                @media (max-width: 768px) {
                    width: 100%;
                }
            }

            .service_button_label {
                font-size: 12px;
                opacity: 0.6;
            }

            .list_content_text {
                padding: 20px;
                font-size: 14px;
                .list_content_text_title {
                    font-size: 18px;
                    margin-bottom: 12px;
                    font-weight: bold;
                    opacity: 1;
                }
                .text_block {
                    opacity: 0.6;
                    margin: 20px 0;
                    font-size: 14px;
                    line-height: 18px;
                    p {
                        margin-bottom: 8px;
                    }
                }
                a {
                    color: #00A0E5;
                    font-weight: bold;
                    &:HOVER {
                        color: #008bc7;
                    }
                    &.block_style {
                        margin: 32px 0;
                    }
                }
                .data_block {
                    margin: 18px 0;
                    padding-bottom: 12px;
                    border-bottom: 2px solid rgba(15, 15, 15, 0.2);
                    &.u--no_border {
                        border-bottom: none;
                        margin-bottom: 32px;
                    }
                    .data_label {
                        opacity: 0.5;
                        margin-bottom: 8px;
                        font-size: 10px;
                        text-transform: uppercase;
                        font-weight: bold;
                    }
                    .data_content {
                        font-size: 14px;
                    }
                    .data_switch {
                        border-radius: 4px;
                        background-color: #00000024;
                        color: #fff;
                        padding: 4px;
                        text-align: center;
                        .data_switch_option {
                            cursor: pointer;
                            border-radius: 4px;
                            display: inline-block;
                            vertical-align: middle;
                            width: 50%;
                            padding: 12px 0;
                            &:first-child {
                                width: 49%;
                                margin-right: 1%;
                            }
                            &:HOVER {
                                background-color: rgba(0,0,0,0.1);
                            }
                            &.active {
                                background-color: #3D87F5;
                                color: #fff;
                            }
                        }
                    }
                }

            }

            .service_button {
                border-radius: 3px;
                cursor: pointer;
                text-align: center;
                display: block;
                margin: 28px 0;
                padding: 18px 0;
                width: 100%;
                color: #fff !important;
                background-color: #3E87F5;
                font-weight: bold;
                &.checked {
                    background-color: #7ba9ed;
                    pointer-events: none;
                }
                &:HOVER {
                    background-color: #246bd5;
                }
            }

            .service_menu_list_content_changelog {
                .changelog_item {
                    margin: 12px 0;
                    padding: 20px 0;
                    &:nth-child(even) {
                        background-color: #EFF1F6;
                        border-top: 2px solid #E7EAF1;
                        border-bottom: 2px solid #E7EAF1;
                    }
                }
                .changelog_date_block, .changelog_items_block {
                    display: inline-block;
                    vertical-align: top;
                }
                .changelog_date_block {
                    width: 112px;
                    text-align: right;
                    font-size: 12px;
                    opacity: 0.7;
                    padding-top: 4px;
                }
                .changelog_item_label {
                    padding: 4px 0px;
                    font-weight: bold;
                    width: 82px;
                    border-radius: 4px;
                    color: #fff;
                    background-color: gray;
                    display: inline-block;
                    font-size: 12px;
                    text-align: center;
                    &.new {
                        background-color: #61C02E;
                    }
                    &.fixed {
                        background-color: #ED9B6B;
                    }
                    &.updated {
                        background-color: #00A0E5;
                    }
                }
                .changelog_items_block {
                    padding: 0 0px 0px 24px;
                    margin-bottom: 12px;
                    .changelog_item_title {
                        font-size: 18px;
                        font-weight: bold;
                        margin-bottom: 12px;
                    }
                    .changelog_item_list {
                        margin-bottom: 20px;
                        &:last-child {
                            margin-bottom: 0;
                        }
                        ul {
                            padding-left: 8px;
                            border-left: 2px solid gray;
                        }

                    }

                    ul {
                        padding-top: 8px;
                        padding-bottom: 4px;
                        font-size: 14px;
                        opacity: 0.8;
                        li {
                            margin-bottom: 8px;
                            &:BEFORE {
                                content: "- ";
                            }
                            &:last-child {
                                margin-bottom: 0;
                            }
                        }
                    }
                }
            }
            b {
                font-weight: bold;
            }
        `;

    }

    componentDidMount() {
        if (!this.props.service_name) {
            console.warn("Missing expected prop 'service_name'!");
        }
    }

    translate(str) {
        if (this.props.language) {
            return oh.translate(translations.prefix + str, undefined, this.props.language);
        }
        return oh.translate(translations.prefix + str);
    }

    getTitleClasses(index) {
        let class_name = 'title_big';
        if (this.state.render_menu_option === -1) {
            return '';
        }
        if (this.state.render_menu_option !== index) {
            class_name = 'hide';
        }
        return class_name;
    }

    hoverContainerClick() {

        let render_menu = this.state.render_menu;
        let first_triggered = this.state.first_triggered;
        let render_menu_option = this.state.render_menu_option;

        if (!this.state.render_menu) {
            render_menu = true;
            first_triggered = true;
        }

        if (this.state.render_menu && this.state.render_menu_option === -1) {
            render_menu = false;
        }

        if (this.state.render_menu && this.state.render_menu_option !== -1) {
            render_menu_option = -1;
            render_menu = true;
        }

        this.setState({
            render_menu,
            first_triggered,
            render_menu_option
        });

    }

    handleResetPassword() {
        this.setState({
            reset_password_clicked: true
        });
    }

    getLanguageFromObject(obj) {
        let lang = this.props.langauge || oh.getLang();
        if (obj.hasOwnProperty(lang)) {
            return obj[lang];
        } else if (obj.hasOwnProperty(oh.getFallbackLang())) {
            return obj[oh.getFallbackLang()];
        }
        console.warn("OH: Changelog entry does not contain specified language or fallback language translation.");
        return "";
    }

    getChangelogEntryTitle(changelog_entry) {
        if (typeof changelog_entry.title === "object" && !Array.isArray(changelog_entry.title)) {
            return this.getLanguageFromObject(changelog_entry.title);
        }
        return changelog_entry.title;
    }

    renderChangeLog() {
        return (
            <div className="service_menu_list_content_changelog">
                {
                    this.props.changelog.map((change_entry, i) => {
                        const change_types = ['new', 'updated', 'fixed'];
                        return (
                            <div className="changelog_item" key={i}>
                                <div className="changelog_date_block">
                                    <p>
                                        {change_entry.date}
                                    </p>
                                </div>
                                <div className="changelog_items_block">
                                    <p className="changelog_item_title">
                                        {this.getChangelogEntryTitle(change_entry)}
                                    </p>
                                </div>

                                {
                                    change_types.map((type, type_index) => {

                                        if (change_entry[type] === undefined) {
                                            return null;
                                        }

                                        return (
                                            <div key={type_index}>
                                                <div className="changelog_date_block">
                                                    <div className={`changelog_item_label ${type}`}>
                                                        {this.translate(type)}
                                                    </div>
                                                </div>
                                                <div className={`changelog_items_block changelog_item_list ${type}`} key={type_index}>
                                                    <ul>
                                                        {
                                                            change_entry[type].map((a_item, z) => {
                                                                return (
                                                                    <li key={z}>
                                                                        {typeof a_item === "object" ? this.getLanguageFromObject(a_item) : a_item}
                                                                    </li>
                                                                );
                                                            })
                                                        }
                                                    </ul>
                                                </div>
                                            </div>

                                        );
                                    })
                                }

                            </div>
                        );
                    })
                }
            </div>
        );
    }



    renderLanguageToggle() {
        //Currently not supported.
        //TODO: Add support in all services for fetching language settings from profile API.
        // return (
        //     <div className="data_block u--no_border">
        //         <p className="data_label">
        //             { this.translate('language') }
        //         </p>
        //         <div className="data_switch">
        //             <div className="data_switch_option active">
        //                 Swedish
        //             </div>
        //             <div className="data_switch_option">
        //                 English
        //             </div>
        //         </div>
        //     </div>
        // );
        return null;
    }

    renderPasswordResetBtns() {
        if (!this.props.enable_password_reset) {
            return null;
        }

        return (
            <div className="data_block u--no_border">
                <div className={`service_button ${this.state.reset_password_clicked ? 'checked' : ''}`} onClick={() => this.handleResetPassword()}>
                    {this.state.reset_password_clicked ? this.translate('sent') : this.translate('change_password')}
                </div>
                <p className="service_button_label">
                    {this.translate('reset_password_info')}: <b>{window.sso.getJWT().getClaim('email')}</b>
                </p>
            </div>
        );
    }

    renderContent() {

        if (this.state.render_menu_option === -1) {
            return null;
        }

        if (this.state.render_menu_option === 0) {
            return this.renderChangeLog();
        }

        if (this.state.render_menu_option === 2) {

            return (
                <div className="service_menu_list_content_contact">
                    <div className="list_content_cover_image">

                    </div>
                    <div className="list_content_text">
                        <p className="list_content_text_title">
                            All Binary
                        </p>
                        <p className="text_block">
                            <p>{this.translate('company_info_1')}</p>
                            <p>{this.translate('company_info_2')}</p>
                            <p>{this.translate('company_info_3')}</p>
                        </p>
                        <a className="block_style" href="https://www.allbinary.se" target="_blank" rel="noreferrer">
                            https://www.allbinary.se
                        </a>
                        <a href="mailto:info@allbinary.se" className="service_button" target="_blank" rel="noreferrer">
                            {this.translate('email_us')}
                        </a>
                    </div>
                </div>
            );
        }


        if (this.state.render_menu_option === 99) {

            const claims = ['first_name', 'last_name', 'username', 'email'];
            let user_info = [];

            claims.forEach((claim, i) => {
                user_info.push({
                    label: this.translate(claim),
                    data: window.sso.getJWT().getClaim(claim)
                });
            });

            return (
                <div className="service_menu_list_content_contact">
                    <div className="list_content_text">
                        <p className="list_content_text_title">
                            {this.translate('account_information')}
                        </p>
                        {
                            user_info.map((item, i) => {
                                return (
                                    <div key={i} className="data_block">
                                        <p className="data_label">
                                            {item.label}
                                        </p>
                                        <p className="data_content">
                                            {item.data}
                                        </p>
                                    </div>
                                );
                            })
                        }
                        {this.renderLanguageToggle()}
                        {this.renderPasswordResetBtns()}
                    </div>
                </div>
            );
        }


    }

    handleLogout() {
        window.sso.logout('https://login.allbin.se/');
    }

    render() {
        return (
            <this.Container className={`${this.state.render_menu ? 'render_menu' : 'disabled'} ${this.state.render_menu_option !== -1 ? 'menu_option_expanded' : ''}`}>
                <div className={this.props.navbar ? 'navbar' : ''}>

                    {
                        this.props.styled_service_name ?
                            <div className="styled_service_name" onClick={() => this.hoverContainerClick()}>
                                {
                                    this.state.render_menu ?
                                        <IconBack />
                                        :
                                        this.props.service_name
                                }
                            </div>
                            :
                            <div className="service_circle" onClick={() => this.hoverContainerClick()}>
                                {
                                    this.state.render_menu ?
                                        <IconRemove />
                                        :
                                        <IconMenu />
                                }
                            </div>
                    }
                </div>

                {
                    this.state.render_menu ?
                        <div className="service_menu_fade" onClick={() => this.setState({ render_menu: false, render_menu_option: -1 })}>
                        </div>
                        :
                        null
                }
                <div className={`service_control ${this.state.render_menu ? 'enabled' : 'disabled'} ${!this.state.first_triggered ? 'hide' : ''}`}>
                    <div className="service_menu">
                        <div className="service_design">
                            <p className="service_design_title">
                                {this.props.service_name}
                            </p>
                            <p className="service_design_subtitle">
                                {this.translate('a_tool_by_allbin')}
                            </p>
                        </div>

                        <div className={`service_menu_list`}>
                            <ul>
                                {
                                    this.props.changelog ?
                                        <li className={`${this.getTitleClasses(0)}`} onClick={() => this.setState({ render_menu_option: 0 })}>
                                            <div className="service_menu_list_icon">
                                                <IconChangelog className="service_menu_list_icon_dot" />
                                            </div>
                                            <div className="service_menu_list_label">
                                                {this.translate('changelog')}
                                            </div>
                                        </li>
                                        :
                                        null
                                }
                                {
                                    this.props.help ?
                                        <li className={`${this.getTitleClasses(1)}`} onClick={() => this.setState({ render_menu_option: 1 })}>
                                            <div className="service_menu_list_icon">
                                                <IconHelp className="service_menu_list_icon_dot" />
                                            </div>
                                            <div className="service_menu_list_label">
                                                {this.translate('help')}
                                            </div>
                                        </li>
                                        :
                                        null
                                }
                                {
                                    this.props.contact ?
                                        <li className={`${this.getTitleClasses(2)}`} onClick={() => this.setState({ render_menu_option: 2 })}>
                                            <div className="service_menu_list_icon">
                                                <IconContact className="service_menu_list_icon_dot" />
                                            </div>
                                            <div className="service_menu_list_label">
                                                {this.translate('contact')}
                                            </div>
                                        </li>
                                        :
                                        null
                                }
                            </ul>
                        </div>
                        <div className={`service_menu_list_content`}>
                            {this.renderContent()}
                        </div>

                        <div className="service_menu_actions">
                            {
                                this.props.show_credentials && this.state.credentials_available ?
                                    <div className={`service_menu_title ${this.state.render_menu_option !== -1 ? 'hide' : ''}`}
                                        onClick={() => this.setState({ render_menu_option: 99 })}>
                                        <p className="service_menu_title_label">
                                            {this.translate('logged_in_as')}
                                        </p>
                                        <p className="service_menu_title_data">
                                            {window.sso.getJWT().getClaim("username")}
                                        </p>
                                    </div>
                                    :
                                    null
                            }
                            {
                                this.props.dashboard_link ?
                                    <a className="blue" href="https://dashboard.allbin.se">
                                        {this.translate('to_dashboard')}
                                    </a>
                                    :
                                    null
                            }
                            <a className="black" onClick={() => this.handleLogout()}>
                                {this.translate('log_out')}
                            </a>
                        </div>
                    </div>
                </div>

            </this.Container>
        );
    }
}

export default ServiceController;
